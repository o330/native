import * as React from 'react'
import PropTypes from 'prop-types'
import {
  Box, Button, FormControl, Input, Text, useToast, View, WarningOutlineIcon,
} from 'native-base'
import { useTranslation } from 'react-i18next'
import { useAuthContext } from '../../context/auth'

const SignInScreen = ({ navigation }) => {
  const { t } = useTranslation()
  const [username, setUsername] = React.useState('')
  const [password, setPassword] = React.useState('')
  const [isInvalid, setIsInvalid] = React.useState(false)
  const { dispatch, signIn } = useAuthContext()
  const toast = useToast()
  return (
    <View flex={1}>
      <Box>
        <FormControl isInvalid={isInvalid} pt={1}>
          <Input
            placeholder={t('login.username')}
            value={username}
            onChangeText={setUsername}
          />
          <Input
            placeholder={t('login.password')}
            value={password}
            onChangeText={setPassword}
            secureTextEntry
          />
          <FormControl.ErrorMessage
            leftIcon={<WarningOutlineIcon size="xs" />}
          >
            {t('login.wrongCredentials')}
          </FormControl.ErrorMessage>
        </FormControl>
      </Box>
      <Button
        onPress={() => signIn({
          dispatch, username, password, setIsInvalid, toast,
        })}
      >{t('login.login')}
      </Button>
      <Text>{t('login.createNewAccountPrompt')}</Text>
      <Button onPress={() => navigation.navigate('SignUp')}>{t('login.createAccount')}</Button>
    </View>
  )
}
export default SignInScreen

SignInScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }),
}

SignInScreen.defaultProps = {
  navigation: {
    navigate: () => null,
  },
}
