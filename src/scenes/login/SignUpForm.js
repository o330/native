import * as React from 'react'
import {
  Button, Input, useToast, View,
} from 'native-base'
import { useTranslation } from 'react-i18next'
import { useAuthContext } from '../../context/auth'

const SignUpForm = () => {
  const { t } = useTranslation()
  const [username, setUsername] = React.useState('')
  const [password, setPassword] = React.useState('')
  const authContext = useAuthContext()
  const { signUp } = authContext
  const { dispatch } = authContext
  const toast = useToast()

  return (
    <View flex={1} pt={1}>
      <Input
        placeholder={t('login.username')}
        value={username}
        onChangeText={setUsername}
      />
      <Input
        placeholder={t('login.password')}
        value={password}
        onChangeText={setPassword}
        secureTextEntry
      />
      <Button
        onPress={() => signUp({
          dispatch, username, password, toast,
        })}
      >{t('login.signup')}
      </Button>
    </View>
  )
}
export default SignUpForm
