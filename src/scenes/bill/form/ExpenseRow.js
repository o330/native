import React, { useEffect, useRef, useState } from 'react'
import {
  Box, FormControl, HStack, Text,
} from 'native-base'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'
import { GeneralInput, RemoveButton } from '../../../components'
import NumericInput from '../../../components/NumericInput'

const NumericInputFormControl = ({
  name, value, setValue, ...props
}) => {
  const ref = useRef(null)
  const [variant, setVariant] = React.useState('unstyled')
  useEffect(() => {
    if (!value && !ref?.current?.isFocused()) {
      setVariant('outline')
    } else setVariant('unstyled')
  }, [value, ref?.current?.isFocused()])
  return (
    <FormControl flex={1} isRequired isInvalid={!value}>
      <NumericInput
        placeholder="0.00"
        value={value}
        setValue={setValue}
        name={name}
        onFocus={() => setVariant('unstyled')}
        onBlur={() => {
          if (!value) setVariant('outline')
        }}
        variant={variant}
        inputElementRef={ref}
        {...props}
      />
    </FormControl>
  )
}
export default function ExpenseRow({ data, dispatch }) {
  const { t } = useTranslation()
  const [expense, setExpense] = useState(data)
  const handleChange = (event) => {
    const { name, text } = event
    setExpense((prevState) => ({
      ...prevState,
      [name]: text,
    }))
  }
  const removeRow = () => {
    dispatch({ type: 'REMOVE_ROW', expense })
  }
  useEffect(() => {
    dispatch({ type: 'UPDATE_EXPENSE', expense })
  }, [expense])

  useEffect(() => {
    setExpense(data)
  }, [data.rowIndex])

  return (
    <Box>
      <RemoveButton onPress={removeRow} />
      <HStack
        alignSelf="center"
        px="4"
        space={1}
      >
        <FormControl flex={3} isRequired isInvalid={!expense.title}>
          <GeneralInput
            autoFocus
            placeholder={t('expenseTitle')}
            value={expense.title}
            onChange={handleChange}
            name="title"
          />
        </FormControl>
        <NumericInputFormControl
          value={expense.quantity}
          setValue={setExpense}
          name="quantity"
        />
        <NumericInputFormControl
          value={expense.unitCost}
          setValue={setExpense}
          name="unitCost"
          InputRightElement={<Text fontSize="sm">zł</Text>}
        />
      </HStack>
    </Box>
  )
}

ExpenseRow.propTypes = {
  data: PropTypes.shape({
    rowIndex: PropTypes.number,
    title: PropTypes.string,
    quantity: PropTypes.string,
    unitCost: PropTypes.string,
  }).isRequired,
  dispatch: PropTypes.func,
}

ExpenseRow.defaultProps = {
  dispatch: () => null,
}

NumericInputFormControl.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.string,
  setValue: PropTypes.func.isRequired,
}

NumericInputFormControl.defaultProps = {
  value: '',
}
