/* eslint-disable react/prop-types */
import React from 'react'
import { Box, Stack, View } from 'native-base'
import { useTranslation } from 'react-i18next'
import PropTypes from 'prop-types'
import { DatePicker } from '../../../components'
import CategoryDropBox from '../../expense/category/CategoryDropBox'
import DefaultImagePicker from '../../../components/DefaultImagePicker'

export default function CommonAttributesForm({ state, dispatch, ...props }) {
  const { t } = useTranslation()
  return (
    <View
      flex={1}
      w="100%"
      {...props}
    >
      <Stack
        space={2}
        alignSelf="center"
        px="1"
        safeArea
        w={{
          base: '100%',
          md: '25%',
        }}
      >
        <DatePicker
          label={t('date') + state?.addedAt?.toLocaleDateString()}
          date={state?.addedAt}
          setDate={(itemValue) => dispatch({ type: 'UPDATE_ADDED_AT', addedAt: itemValue })}
        />
        <Box>
          <CategoryDropBox
            selectedValue={state?.category?.id}
            onValueChange={(itemValue) => dispatch({ type: 'UPDATE_CATEGORY', category: itemValue })}
          />
        </Box>
        <DefaultImagePicker onSuccess={(result) => dispatch({ type: 'SET_IMAGE', image: result.uri })} uri={state.image} />
      </Stack>
    </View>
  )
}

CommonAttributesForm.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }),
}

CommonAttributesForm.defaultProps = {
  navigation: {
    navigate: () => null,
  },
}
