/* eslint-disable react/prop-types */
import React from 'react'
import { Heading, View } from 'native-base'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'
import ExpenseRow from './ExpenseRow'

export default function ExpenseRowList({
  navigation, dispatch, expenses, ...props
}) {
  const { t } = useTranslation()

  return (
    <View
      flex={1}
      justifyContent="center"
      alignItems="center"
      {...props}
    >
      <Heading>{t('createBillSecondStepHeading')}</Heading>
      <View
        flex={1}
        w="100%"
      >
        {expenses && expenses.map((value) => <ExpenseRow key={value.rowIndex} data={value} dispatch={dispatch} />)}
      </View>
    </View>
  )
}

ExpenseRowList.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }),
}

ExpenseRowList.defaultProps = {
  navigation: {
    navigate: () => null,
  },
}
