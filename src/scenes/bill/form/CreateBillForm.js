/* eslint-disable react/prop-types */
import React from 'react'
import PropTypes from 'prop-types'
import { Button, HStack, ScrollView } from 'native-base'
import { useTranslation } from 'react-i18next'
import CommonAttributesForm from './CommonAttributesForm'
import { BillsProvider, useBillsService } from '../../../services/BillsProvider'
import LoadingScreen from '../../splash'
import ExpenseRowList from './ExpenseRowList'
import { Submit } from '../../expense/buttons'
import useBillFormReducer from './useBillFormReducer'
import { useAuthContext } from '../../../context/auth'
import { KeyboardAvoidingView } from '../../../components'

function CreateBillForm({ navigation, route }) {
  const [state, dispatch] = useBillFormReducer({ route })
  const { t } = useTranslation()
  const controller = new AbortController()
  const billsService = useBillsService()
  const { userToken } = useAuthContext()
  const onSubmit = () => {
    dispatch({
      type: 'SUBMIT', signal: controller.signal, billsService, userToken, navigation,
    })
  }

  if (state.submit) return <LoadingScreen />
  return (
    <KeyboardAvoidingView flex={1}>
      <ScrollView>
        <CommonAttributesForm
          state={state}
          dispatch={dispatch}
        />
        <ExpenseRowList
          navigation={navigation}
          expenses={state.expenses}
          dispatch={dispatch}
        />
      </ScrollView>
      <HStack
        space={20}
        justifyContent="center"
        px="4"
        pt="3"
        pb="1"
      >
        <Button isDisabled={state.isDisabled} alignSelf="center" onPress={() => { dispatch({ type: 'ADD_ROW' }) }}>{t('addRow')}</Button>
        <Submit
          isDisabled={state.isDisabled}
          alignSelf="flex-end"
          onPress={onSubmit}
        />
      </HStack>
    </KeyboardAvoidingView>
  )
}

export default ({ navigation, route }) => (
  <BillsProvider>
    <CreateBillForm navigation={navigation} route={route} />
  </BillsProvider>
)

CreateBillForm.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }),
}

CreateBillForm.defaultProps = {
  navigation: {
    navigate: () => null,
  },
}
