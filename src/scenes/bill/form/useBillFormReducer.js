import { useReducer } from 'react'
import * as MediaLibrary from 'expo-media-library'

const getBill = async (state) => {
  const {
    id, addedAt, category, shop, expenses, image,
  } = state
  const exp = expenses.map((value) => {
    const {
      id: expId, title, quantity, unitCost, currency,
    } = value
    console.log({
      id: expId, title, category, quantity, unitCost, currency,
    })
    return {
      id: expId, title, category, quantity, unitCost, currency,
    }
  })
  const bill = {
    id,
    addedAt,
    shop,
    expenses: exp,
  }
  if (image) {
    if (image.includes('cache')) {
      const { uri } = await MediaLibrary.createAssetAsync(image)
      bill.imageUri = uri
    } else bill.imageUri = image
  }
  return bill
}
const createBill = (state, action) => {
  getBill(state).then((bill) => {
    const {
      billsService, navigation, userToken, signal,
    } = action
    billsService.createBill(bill, userToken, signal).then(() => {
      navigation.navigate('Home')
    }).catch((error) => {
      console.log(error)
    })
  })
  return ({
    submit: true,
  })
}
const updateExpenses = (state, action) => {
  const { expense } = action
  const updatedExpenses = state.expenses.map((exp) => {
    if (exp.rowIndex === expense.rowIndex) {
      return expense
    }
    return exp
  })
  const isDisabled = updatedExpenses.find((item) => !item.unitCost || !item.title || !item.quantity)
  return {
    ...state,
    expenses: updatedExpenses,
    isDisabled,
  }
}
const addExpenseRow = (state) => {
  const expense = {
    rowIndex: state.rowIdCounter,
    title: '',
    currency: 'PLN',
  }
  return {
    ...state,
    rowIdCounter: state.rowIdCounter + 1,
    expenses: [...state.expenses, expense],
    isDisabled: true,
  }
}
const removeExpenseRow = (state, action) => {
  const { expense } = action
  if (state.expenses.length > 1) {
    const slice = state.expenses.slice()
    const filtered = slice
      .filter((value) => value.rowIndex !== expense.rowIndex)
    const isDisabled = filtered.find((item) => !item.unitCost || !item.title || !item.quantity)
    return {
      ...state,
      expenses: filtered,
      isDisabled,
    }
  }
  return state
}
const reducer = (state, action) => {
  switch (action.type) {
    case 'UPDATE_CATEGORY':
      return {
        ...state,
        category: { id: action.category },
      }
    case 'UPDATE_ADDED_AT':
      return {
        ...state,
        addedAt: action.addedAt,
      }
    case 'UPDATE_EXPENSE':
      return updateExpenses(state, action)
    case 'ADD_ROW':
      return addExpenseRow(state)
    case 'REMOVE_ROW':
      return removeExpenseRow(state, action)
    case 'SET_IMAGE':
      return {
        ...state,
        image: action.image,
      }
    case 'SUBMIT':
      return createBill(state, action)
    default:
      throw new Error()
  }
}
const initFunc = ({ route }) => {
  const bill = route?.params?.bill
  if (bill) {
    let rowIdCounter = 0
    const expenses = bill.expenses.map((item) => {
      const expense = item
      expense.rowIndex = expense.id
      expense.quantity = expense.quantity.toString()
      expense.unitCost = expense.unitCost.toString()
      if (rowIdCounter <= expense.rowIndex) rowIdCounter = expense.rowIndex + 1
      return expense
    })
    const { category } = expenses[0]
    return {
      submit: false,
      ...bill,
      addedAt: new Date(Date.parse(bill.addedAt)),
      expenses,
      rowIdCounter,
      category,
      image: bill.imageUri,
      isDisabled: false,
    }
  }
  return {
    submit: false,
    image: route?.params?.image,
    addedAt: new Date(),
    shop: null,
    category: { id: null },
    rowIdCounter: 1,
    expenses: [{
      rowIndex: 0,
      title: '',
      quantity: '1',
      unitCost: '',
      currency: 'PLN',
    }],
    isDisabled: false,
  }
}

export default function useBillFormReducer(props) {
  return useReducer(reducer, props, initFunc)
}
