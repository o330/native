import * as React from 'react'
import { useNavigation } from '@react-navigation/native'
import {
  Box, Collapse, HStack, Image, Pressable, Spacer, Text,
} from 'native-base'
import PropTypes from 'prop-types'
import { useBillsService } from '../../services/BillsProvider'
import { useAuthContext } from '../../context/auth'
import { EditButton } from '../../components'
import { DeleteButton } from '../expense/buttons'
import ExpensesList from '../expense/ExpensesList'
import { currencyFormat } from '../../i18n/language'

export default function Bill({ item: bill, index, flatLisRef }) {
  const [show, setShow] = React.useState(false)
  const billsService = useBillsService()
  const { userToken } = useAuthContext()
  const navigation = useNavigation()
  const deleteItem = async () => {
    await billsService.deleteBill(bill.id, userToken).then((response) => {
      navigation.navigate('Home', { deletedItemId: response.data })
    })
  }
  const onPress = () => {
    setShow(!show)
    if (!show) flatLisRef.current.scrollToIndex({ animated: true, index })
  }
  const getCurrencySum = (expenses) => {
    const sum = expenses.length > 0 ? expenses.map((v) => v.quantity * v.unitCost)
      .reduce((previousValue, currentValue) => previousValue + currentValue) : 0.0
    return currencyFormat.format(sum)
  }
  return (
    <Pressable flex={1} alignItems="center" onPress={() => onPress()}>{({ isPressed }) => (
      <Box
        bg={isPressed ? 'cyan.900' : 'cyan.700'}
        p="5"
        rounded="8"
        style={{
          transform: [
            {
              scale: isPressed ? 0.96 : 1,
            },
          ],
        }}
      >
        <HStack alignItems="stretch">
          <HStack alignItems="stretch" space={2}>
            <Image key={Date.now()} source={{ uri: bill.imageUri }} style={{ width: 20, height: 20 }} />
            <Text fontSize={10} color="cyan.100">
              {new Date(bill.addedAt).toLocaleDateString()}
            </Text>
          </HStack>
          <Spacer />
          <EditButton onPress={() => { navigation.navigate('BillForm', { bill }) }} />
          <DeleteButton onPress={deleteItem} />
        </HStack>
        <HStack>
          <Text color="cyan.50" mt="3" fontWeight="medium" fontSize={20} bold>
            {bill.expenses[0].category.name}
          </Text>
          <Spacer />
          <Text alignSelf="flex-end" fontSize="sm">
            {getCurrencySum(bill.expenses)}
          </Text>
        </HStack>
        <Collapse
          isOpen={show}
        >
          <ExpensesList data={bill.expenses} key={bill.expenses.join()} />
        </Collapse>
      </Box>
    )}
    </Pressable>
  )
}

Bill.propTypes = {
  flatLisRef: PropTypes.shape({ current: PropTypes.shape({ scrollToIndex: PropTypes.func }) }),
  index: PropTypes.number,
  item: PropTypes.shape(
    {
      id: PropTypes.number,
      addedAt: PropTypes.string,
      imageUri: PropTypes.string,
      shop: PropTypes.shape(),
      expenses: PropTypes.arrayOf(PropTypes.shape(
        {
          id: PropTypes.number,
          category: PropTypes.shape(),
          title: PropTypes.string,
          quantity: PropTypes.number,
          unitCost: PropTypes.number,
        },
      )),
    },
  ),
}
Bill.defaultProps = {
  flatLisRef: { current: { scrollToIndex: () => null } },
  index: null,
  item: null,
}
