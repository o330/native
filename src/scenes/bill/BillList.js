import * as React from 'react'
import { useRef } from 'react'
import { Divider, FlatList, View } from 'native-base'
import PropTypes from 'prop-types'
import Bill from './Bill'

export default function BillList({ bills }) {
  const refContainer = useRef(null)
  return (
    <FlatList
      px="4"
      data={bills}
      marginBottom={-12}
      ref={refContainer}
      renderItem={({ item, index }) => (
        <View>
          <Bill item={item} index={index} flatLisRef={refContainer} />
          <Divider />
        </View>
      )}
      keyExtractor={(item) => item.id}
    />
  )
}
BillList.propTypes = {
  bills: PropTypes.arrayOf(PropTypes.shape(
    {
      id: PropTypes.number,
      addedAt: PropTypes.string,
      shop: PropTypes.shape(),
      expenses: PropTypes.arrayOf(PropTypes.shape(
        {
          id: PropTypes.number,
          category: PropTypes.shape({}),
          title: PropTypes.string,
          quantity: PropTypes.number,
          unitCost: PropTypes.number,
        },
      )),
    },
  )),
}
BillList.defaultProps = {
  bills: [],
}
