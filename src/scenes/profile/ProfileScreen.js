import React, { useEffect, useState } from 'react'
import {
  Avatar, Center, Divider, Flex, HStack, Spacer, Text, View,
} from 'native-base'
import { useIsFocused } from '@react-navigation/native'
import { useTranslation } from 'react-i18next'
import { TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import Icon from 'react-native-vector-icons/FontAwesome'
import { useAuthContext } from '../../context/auth'
import { ExpensesStatsProvider, useExpensesStats } from '../../services/ExpenseStatsProvider'
import { getCurrencySum, getStats } from '../stats/StatsUtil'

const TouchableStat = ({
  label, navigation, value, data,
}) => (
  <TouchableOpacity flex={1} onPress={() => navigation.navigate('ExpenseStatsScreen', { data })}>
    <HStack space={1} alignItems="flex-end">
      <Text fontSize="2xl">{label}: {value}</Text>
      <Spacer />
      <Icon size={30} color="white" name="bar-chart" />
    </HStack>
  </TouchableOpacity>
)
const Profile = ({ navigation }) => {
  const {
    getTotalSpending,
    getPeriodSpending,
    getMonthParams,
    getWeekParams,
  } = useExpensesStats()

  const [total, setTotal] = useState({ values: [] })
  const [month, setMonth] = useState({ values: [] })
  const [week, setWeek] = useState({ values: [] })
  const [today, setToday] = useState({ values: [] })
  const { username, userToken } = useAuthContext()
  const isFocused = useIsFocused()
  const { t } = useTranslation()
  const controller = new AbortController()

  useEffect(() => {
    if (isFocused) {
      const { signal } = controller
      getStats(getTotalSpending, {}, userToken, signal, setTotal)
      getStats(getPeriodSpending, getMonthParams(), userToken, signal, setMonth)
      getStats(getPeriodSpending, getWeekParams(), userToken, signal, setWeek)
      getStats(getPeriodSpending, { from: new Date(), to: new Date() }, userToken, signal, setToday)
    }
    return () => controller.abort()
  }, [isFocused])

  return (
    <View flex={1}>
      <Center mx="10" flex={1} py="3" justifyContent="flex-start">
        <Avatar
          size="lg"
          bg="green.500"
        >
          {username}
        </Avatar>
        <Flex flex={0.1} />
        <Text fontSize="2xl" bold>{t('yourExpenses')}:</Text>
        <Flex flex={0.05} />
        <TouchableStat value={getCurrencySum(total.values)} label={t('total')} navigation={navigation} data={total} />
        <Divider bg="darkBlue.700" thickness="2" />
        <TouchableStat value={getCurrencySum(month.values)} label={t('month')} navigation={navigation} data={month} />
        <Divider bg="darkBlue.700" thickness="3" />
        <TouchableStat value={getCurrencySum(week.values)} label={t('week')} navigation={navigation} data={week} />
        <Divider bg="darkBlue.700" thickness="4" />
        <TouchableStat value={getCurrencySum(today.values)} label={t('today')} navigation={navigation} data={today} />
      </Center>
    </View>
  )
}

export default function ProfileScreen({ navigation }) {
  return (
    <ExpensesStatsProvider>
      <Profile navigation={navigation} />
    </ExpensesStatsProvider>
  )
}
ProfileScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }),
}
ProfileScreen.defaultProps = {
  navigation: { navigate: () => null },
}

Profile.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }),
}
Profile.defaultProps = {
  navigation: { navigate: () => null },
}
TouchableStat.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }),
  label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  data: PropTypes.arrayOf(PropTypes.shape({
    category: PropTypes.shape({
      name: PropTypes.string,
      colorScheme: PropTypes.string,
    }),
    value: PropTypes.number,
  })).isRequired,
}
TouchableStat.defaultProps = {
  navigation: { navigate: () => null },
}
