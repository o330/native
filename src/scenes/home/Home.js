/* eslint-disable react/prop-types */
import * as React from 'react'
import { useState } from 'react'
import PropTypes from 'prop-types'
import { Divider, Heading, View } from 'native-base'
import { useTranslation } from 'react-i18next'
import AddButton from '../expense/buttons/AddButton'
import { BillsProvider } from '../../services/BillsProvider'
import BillList from '../bill'
import Filter from './components/Filter'

const HomeScreen = ({ navigation, route }) => {
  const { t } = useTranslation()
  const [bills, setBills] = useState([])

  return (
    <View
      flex="1"
      justifyContent="center"
      alignItems="center"
    >
      <Heading>{t('yourExpenses')}</Heading>
      <Filter setBills={setBills} route={route} />
      <Divider my={2} />
      <BillList bills={bills} />
      <AddButton navigation={navigation} />
    </View>
  )
}
export default ({ navigation, route }) => (
  <BillsProvider>
    <HomeScreen navigation={navigation} route={route} />
  </BillsProvider>
)

HomeScreen.propTypes = {
  navigation: PropTypes.shape({
    dispatch: PropTypes.func,
  }),
}

HomeScreen.defaultProps = {
  navigation: {
    dispatch: () => null,
  },
}
