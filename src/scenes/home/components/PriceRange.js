import React from 'react'
import { HStack, Text } from 'native-base'
import { useTranslation } from 'react-i18next'
import PropTypes from 'prop-types'
import NumericInput from '../../../components/NumericInput'

export default function PriceRange({ setPriceRange, priceRange }) {
  const { t } = useTranslation()
  return (
    <HStack alignSelf="center" space={1} mx={3} alignItems="center">
      <Text>{t('from')}</Text>
      <NumericInput
        flex={1}
        placeholder="0.00"
        value={priceRange.from}
        setValue={setPriceRange}
        name="from"
        InputRightElement={<Text fontSize="sm">zł</Text>}
      />
      <Text>{t('to')}</Text>
      <NumericInput
        flex={1}
        placeholder="0.00"
        value={priceRange.to}
        setValue={setPriceRange}
        name="to"
        InputRightElement={<Text fontSize="sm">zł</Text>}
      />
    </HStack>
  )
}
PriceRange.propTypes = {
  priceRange: PropTypes.shape({
    from: PropTypes.string,
    to: PropTypes.string,
  }).isRequired,
  setPriceRange: PropTypes.func.isRequired,
}
