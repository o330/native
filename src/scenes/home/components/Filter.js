import React, { useEffect, useState } from 'react'
import {
  Button, ChevronDownIcon, ChevronUpIcon, Collapse, HStack, Text, useToast,
} from 'native-base'
import PropTypes from 'prop-types'
import { TouchableOpacity } from 'react-native'
import { useTranslation } from 'react-i18next'
import { useIsFocused } from '@react-navigation/native'
import { TimePeriodPicker } from '../../../components'
import { ConnectionIssueToast as showConnectionIssueToast } from '../../../components/error'
import { useAuthContext } from '../../../context/auth'
import { useBillsService } from '../../../services/BillsProvider'
import CategoryDropBox from '../../expense/category/CategoryDropBox'
import PriceRange from './PriceRange'

const getStartDate = () => {
  const date = new Date()
  date.setDate(date.getDate() - 7)
  return date
}
export default function Filter({ setBills, route }) {
  const { t } = useTranslation()
  const [show, setShow] = useState(false)
  const [start, setStart] = useState(getStartDate())
  const [end, setEnd] = useState(new Date())
  const [shop, setShop] = useState(null)
  const [categoryId, setCategoryId] = useState(null)
  const [priceRange, setPriceRange] = useState({})
  const isFocused = useIsFocused()
  const authContext = useAuthContext()
  const billsService = useBillsService()
  const controller = new AbortController()
  const toast = useToast()
  const applyFilter = () => {
    const params = {
      categoryId,
      shop,
      dateRange: {
        start,
        end,
      },
      priceRange,
    }
    billsService.getBills(params, authContext.userToken, controller.signal)
      .then((response) => {
        setBills(response.data)
      }).catch((error) => {
        if (error?.message !== 'canceled') {
          showConnectionIssueToast(toast)
        }
      })
  }

  useEffect(() => {
    applyFilter()
    return () => controller.abort()
  }, [isFocused, route?.params?.deletedItemId])
  useEffect(() => () => controller.abort(), [])

  return (
    <>
      <Collapse
        isOpen={show}
      >
        <TimePeriodPicker
          endDate={end}
          setEndDate={setEnd}
          startDate={start}
          setStartDate={setStart}
        />
        <CategoryDropBox
          selectedValue={categoryId?.id}
          onValueChange={setCategoryId}
          additionalCategories={[{ id: null, name: t('category.all') }, { id: '-1', name: t('category.uncategorized') }]}
        />
        <PriceRange priceRange={priceRange} setPriceRange={setPriceRange} />
        <Button alignSelf="flex-end" onPress={applyFilter}>{t('common.apply')}</Button>
      </Collapse>
      <TouchableOpacity style={{ alignItems: 'flex-end', alignSelf: 'flex-start' }} onPress={() => setShow(!show)}>
        <HStack space={1} mx={3}>
          <Text fontSize="sm">{show ? t('filter.hideFilters') : t('filter.showFilters')}</Text>
          {show ? <ChevronUpIcon mt="1.5" size="4" /> : <ChevronDownIcon mt="1.5" size="4" />}
        </HStack>
      </TouchableOpacity>
    </>
  )
}

Filter.propTypes = {
  setBills: PropTypes.func.isRequired,
  route: PropTypes.shape({
    params: PropTypes.shape({}),
  }),
}
Filter.defaultProps = {
  route: null,
}
