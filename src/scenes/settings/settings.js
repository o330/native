import * as React from 'react'
import { useState } from 'react'
import { useTranslation } from 'react-i18next'
import {
  CheckIcon, FormControl, Select, View,
} from 'native-base'

const SettingsScreen = () => {
  const { t, i18n } = useTranslation()
  const [currentLanguage, setLanguage] = useState(i18n.language)

  const changeLanguage = (value) => {
    i18n
      .changeLanguage(value)
      .then(() => setLanguage(value))
      .catch((err) => console.log(err))
  }

  return (
    <View flex="1" justifyContent="center" alignItems="center">
      <FormControl flex="1" justifyContent="center" alignItems="center">
        <FormControl.Label>{t('languageSelectionLabel')}: </FormControl.Label>
        <Select
          selectedValue={currentLanguage}
          minWidth="200"
          accessibilityLabel="Choose Language"
          placeholder="Choose Language"
          _selectedItem={{
            bg: 'teal.600',
            endIcon: <CheckIcon size="5" />,
          }}
          mt={1}
          onValueChange={changeLanguage}
        >
          <Select.Item label="Polski" value="pl" />
          <Select.Item label="English" value="en" />
        </Select>
      </FormControl>
    </View>
  )
}
export default SettingsScreen
