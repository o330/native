import { useTheme } from 'native-base'
import { currencyFormat } from '../../i18n/language'

export const mapToChartData = (data) => {
  const theme = useTheme()
  return data.map((v) => {
    const { category, value } = v
    const { colorScheme, name } = category
    const fill = theme.colors[colorScheme]['300']
    return {
      x: name, y: value, name, fill, symbol: { fill }, labels: { fill: 'white' }, colorScheme,
    }
  })
}

export function getStats(getSpending, params, userToken, signal, setState) {
  getSpending(params, userToken, signal)
    .then((response) => {
      const values = response.data.sort((a, b) => a.category.name.localeCompare(b.category.name))
      setState({ ...params, values })
    })
    .catch((error) => console.log(error))
}

export function trimName(name) {
  return name.length > 20 ? `${name.substring(0, 18)}...` : name
}

export function trimNames(input) {
  return input.map((datum) => {
    const name = trimName(datum.name)
    return {
      ...datum,
      name,
    }
  })
}
export function getCurrencySum(values) {
  const sum = values.length > 0 ? values.map((v) => v.value)
    .reduce((previousValue, currentValue) => previousValue + currentValue) : 0.0
  return currencyFormat.format(sum)
}
