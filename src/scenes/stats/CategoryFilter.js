import React, { useState } from 'react'
import {
  Box, Checkbox, Collapse, Pressable, Text,
} from 'native-base'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'

export default function CategoryFilter({ categories, onChange, ...props }) {
  const { t } = useTranslation()
  const [show, setShow] = useState(false)
  const [groupValues, setGroupValues] = React.useState(categories.map((c) => c.category))
  return (
    <Pressable pt={8} onPress={() => setShow(!show)} {...props}>
      {({ isPressed }) => (
        <Box
          borderColor="grey"
          borderWidth="1"
          rounded="10"
          p={3}
          style={{
            transform: [
              {
                scale: isPressed ? 0.96 : 1,
              },
            ],
          }}
        >
          <Text fontSize="sm">{t('chooseCategories')}</Text>
          <Collapse isOpen={show}>
            <Checkbox.Group
              onChange={(values) => {
                onChange(values)
                setGroupValues(values)
              }}
              value={groupValues}
              accessibilityLabel="choose numbers"
            >
              {categories && categories.map((v) => {
                const { category } = v
                const { colorScheme } = v
                return (
                  <Checkbox value={category} key={category} colorScheme={colorScheme}>
                    {category}
                  </Checkbox>
                )
              })}
            </Checkbox.Group>
          </Collapse>
        </Box>
      )}
    </Pressable>
  )
}
CategoryFilter.propTypes = {
  categories: PropTypes.arrayOf(PropTypes.shape({
    category: PropTypes.string,
    colorScheme: PropTypes.string,
  })).isRequired,
  onChange: PropTypes.func,
}
CategoryFilter.defaultProps = {
  onChange: () => null,
}
