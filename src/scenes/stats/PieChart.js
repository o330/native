import { VictoryPie } from 'victory-native'
import React from 'react'
import PropTypes from 'prop-types'
import { trimName } from './StatsUtil'

export default function PieChart({ windowWidth, chartHeight, renderData }) {
  return (
    <VictoryPie
      width={windowWidth - 15}
      height={chartHeight}
      style={{
        labels: { fill: 'white', fontSize: 10, fontWeight: 'bold' },
        data: {
          fill: ({ datum }) => (datum.fill),
        },
      }}
      labelPlacement={() => 'parallel'}
      labels={({ datum }) => trimName(datum.name)}
      labelRadius={({ innerRadius }) => innerRadius + 35}
      data={renderData}
    />
  )
}

PieChart.propTypes = {
  windowWidth: PropTypes.number.isRequired,
  chartHeight: PropTypes.number.isRequired,
  renderData: PropTypes.arrayOf(PropTypes.shape()).isRequired,
}
