import { VictoryAxis, VictoryBar, VictoryChart } from 'victory-native'
import React from 'react'
import PropTypes from 'prop-types'
import { Dimensions } from 'react-native'
import { View } from 'native-base'

export default function BarChart({ windowWidth, chartHeight, renderData }) {
  return (
    <View w={windowWidth - 15} h={chartHeight}>
      <VictoryChart width={windowWidth - 15} height={chartHeight}>
        <VictoryAxis
          dependentAxis
          crossAxis
          style={{
            axis: { stroke: 'white' },
            tickLabels: { fill: 'white' },
            ticks: { fill: 'white' },
            grid: { stroke: () => ('grey') },
          }}
        />
        <VictoryBar
          style={{
            data: {
              fill: ({ datum }) => (datum.fill),
            },
          }}
          data={renderData}
          barRatio={0.8}
        />
        <VictoryAxis
          style={{
            axis: {
              stroke: 'white',
              fill: 'white',
            },
            tickLabels: { fill: 'transparent' },
            ticks: { fill: 'white' },
            grid: { stroke: () => ('transparent') },
          }}
          domainPadding={{ x: [50, 50] }}
        />
      </VictoryChart>

    </View>
  )
}

BarChart.propTypes = {
  windowWidth: PropTypes.number,
  chartHeight: PropTypes.number,
  renderData: PropTypes.arrayOf(PropTypes.shape()).isRequired,
}
BarChart.defaultProps = {
  windowWidth: Dimensions.get('window').width,
  chartHeight: (Dimensions.get('window').height / 2) - 20,
}
