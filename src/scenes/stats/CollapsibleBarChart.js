import React from 'react'
import {
  ChevronDownIcon, Collapse, Divider, HStack, Spacer, Text, View,
} from 'native-base'
import { TouchableOpacity } from 'react-native'
import { useTranslation } from 'react-i18next'
import BarChart from './BarChart'

export default function CollapsibleBarChart({
  renderData, show, setShow, ...props
}) {
  const { t } = useTranslation()
  return (
    <View {...props}>
      <TouchableOpacity style={{ alignItems: 'flex-end' }} onPress={() => setShow(!show)}>
        <HStack space={1} mx={3}>
          <Text fontSize="sm">{show ? t('collapsibleBarChart.hidePreview') : t('collapsibleBarChart.showPreview')}</Text>
          <ChevronDownIcon mt="1.5" size="4" />
        </HStack>
      </TouchableOpacity>
      <Collapse
        isOpen={show}
      >
        <BarChart renderData={renderData} />
      </Collapse>
      <Spacer />
      <Divider thickness="3" />
    </View>
  )
}
