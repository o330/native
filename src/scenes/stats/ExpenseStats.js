/* eslint-disable dot-notation */
import React, { useState } from 'react'
import {
  Center, CheckIcon, FormControl, HStack, ScrollView, Select, Text,
} from 'native-base'
import { VictoryLegend } from 'victory-native'
import { Dimensions } from 'react-native'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'
import CategoryFilter from './CategoryFilter'
import BarChart from './BarChart'
import PieChart from './PieChart'
import { getCurrencySum, mapToChartData, trimNames } from './StatsUtil'

export default function ExpenseStats({ initialData }) {
  const { t } = useTranslation()
  const windowWidth = Dimensions.get('window').width
  const chartHeight = (Dimensions.get('window').height / 2) - 20
  const data = mapToChartData(initialData)
  const categories = data.map((v) => ({
    category: v.x,
    colorScheme: v.colorScheme,
  }))
  const [renderData, setRenderData] = useState(data)
  const [chart, setChart] = useState('bar')

  const filterData = (groupValues) => {
    const slice = data.slice()
    const filtered = slice.filter((d) => groupValues.find((element) => element === d.x))
    setRenderData(filtered)
  }

  return (
    <ScrollView flex={1}>
      <Center mx="5" flex={1} justifyContent="flex-start" pb={5}>
        <HStack
          w="100%"
          justifyContent="space-between"
        >
          <FormControl w="35%">
            <FormControl.Label>{t('selectChart')}:</FormControl.Label>
            <Select
              selectedValue={chart}
              minWidth="100"
              mt={1}
              onValueChange={(itemValue) => setChart(itemValue)}
              _selectedItem={{
                bg: 'teal.600',
                endIcon: <CheckIcon size={5} />,
              }}
            >

              <Select.Item label={t('barChart')} value="bar" />
              <Select.Item label={t('pieChart')} value="pie" />
            </Select>
          </FormControl>
          <CategoryFilter categories={categories} onChange={filterData} w="55%" />
        </HStack>
        <Text pt={6} mb={-1} bold>{t('total')}: {getCurrencySum(renderData.map(((item) => ({ value: item.y }))))}</Text>
        {chart === 'bar' ? <BarChart chartHeight={chartHeight} windowWidth={windowWidth} renderData={renderData} />
          : <PieChart chartHeight={chartHeight} windowWidth={windowWidth} renderData={renderData} />}
        <VictoryLegend
          height={30 * (renderData.length / 2) + (renderData.length % 2) * 30}
          itemsPerRow={2}
          orientation="horizontal"
          gutter={50}
          style={{ border: { stroke: 'transparent' } }}
          data={trimNames(renderData)}
        />
      </Center>
    </ScrollView>
  )
}
ExpenseStats.propTypes = {
  initialData: PropTypes.arrayOf(PropTypes.shape()).isRequired,
}
