/* eslint-disable dot-notation */
import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { ScrollView } from 'native-base'
import { TimePeriodPicker } from '../../components'
import { getStats } from './StatsUtil'
import { useAuthContext } from '../../context/auth'
import { ExpensesStatsProvider, useExpensesStats } from '../../services/ExpenseStatsProvider'
import ExpenseStats from './ExpenseStats'

const ExpenseStatsScreen = ({ route }) => {
  const [startDate, setStartDate] = useState(route?.params?.data.from)
  const [endDate, setEndDate] = useState(route?.params?.data.to)
  const [data, setData] = useState(route?.params?.data)
  const { getPeriodSpending } = useExpensesStats()
  const { userToken } = useAuthContext()
  const controller = new AbortController()
  useEffect(() => {
    if (startDate && endDate) {
      getStats(getPeriodSpending, { from: startDate, to: endDate }, userToken, controller.signal, setData)
    }
  }, [startDate, endDate])
  useEffect(() => {
    if (!route?.params?.data.from) {
      setStartDate(null)
    }
    if (!route?.params?.data.to) {
      setEndDate(null)
    }
  }, [route?.params?.data.from, route?.params?.data.to])
  return (
    <ScrollView flex={1}>
      <TimePeriodPicker
        endDate={endDate}
        setEndDate={setEndDate}
        startDate={startDate}
        setStartDate={setStartDate}
      />
      <ExpenseStats initialData={data?.values} key={data?.from?.toString() + data?.to?.toString()} />
    </ScrollView>
  )
}
// eslint-disable-next-line react/prop-types
export default ({ route }) => (
  <ExpensesStatsProvider>
    <ExpenseStatsScreen route={route} />
  </ExpensesStatsProvider>
)
ExpenseStatsScreen.propTypes = {
  route: PropTypes.shape({
    params: PropTypes.shape({
      data: PropTypes.shape({
        from: PropTypes.shape(),
        to: PropTypes.shape(),
        values: PropTypes.arrayOf(PropTypes.shape()),
      }).isRequired,
    }),
  }).isRequired,
}
