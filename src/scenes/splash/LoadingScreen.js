import { View } from 'react-native'
import * as React from 'react'
import { Heading, HStack, Spinner } from 'native-base'
import { useTranslation } from 'react-i18next'

const LoadingScreen = () => {
  const { t } = useTranslation()
  return (
    <View flex={1} alignItems="center" justifyContent="center">
      <HStack space={2} alignItems="center">
        <Spinner accessibilityLabel="Loading" size="lg" />
        <Heading color="primary.500" fontSize="2xl">
          {t('loadingScreen.header')}
        </Heading>
      </HStack>
    </View>
  )
}
export default LoadingScreen
