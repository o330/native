import React, { useEffect, useRef, useState } from 'react'
import { Camera } from 'expo-camera'
import { Text, View } from 'native-base'
import { useIsFocused } from '@react-navigation/native'
import { Entypo } from '@expo/vector-icons'
import { TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'

export default function CameraScreen({ navigation }) {
  const { t } = useTranslation()
  const [hasPermission, setHasPermission] = useState(null)
  const camera = useRef(null)
  const isFocused = useIsFocused()
  useEffect(() => {
    (async () => {
      const { status } = await Camera.requestCameraPermissionsAsync()
      setHasPermission(status === 'granted')
    })()
  }, [])
  const takePicture = () => {
    if (camera) {
      camera.current.takePictureAsync()
        .then((p) => {
          const { uri } = p
          navigation.navigate('BillForm', { image: uri })
        })
    }
  }

  if (hasPermission === null || !isFocused) {
    return <View />
  }
  if (hasPermission === false) {
    return <Text>{t('cameraScreen.missingPermission')}</Text>
  }

  return (
    <View flex={1}>
      <Camera style={{ flex: 1 }} type={Camera.Constants.Type.back} ref={camera}>
        <View flex={1} bgColor="transparent" alignItems="center" justifyContent="flex-end" pb={10}>
          <TouchableOpacity onPress={takePicture}>
            <Entypo name="circle" size={90} color="white" />
          </TouchableOpacity>
        </View>
      </Camera>
    </View>
  )
}

CameraScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }),
}
CameraScreen.defaultProps = {
  navigation: { navigate: () => null },
}
