import React from 'react'
import { View } from 'native-base'
import { Dimensions, ImageBackground } from 'react-native'
import { Resize } from '../../components'

export default function FullScreenImage({ navigation, route }) {
  const windowWidth = Dimensions.get('window').width
  const windowHeight = Dimensions.get('window').height
  const uri = route?.params?.uri
  return (
    <View flex={1} mt={50}>
      <ImageBackground
        key={uri}
        source={{ uri }}
        resizeMode="contain"
        style={{
          position: 'absolute', width: windowWidth, height: windowHeight, top: 0, left: 0,
        }}
      >
        <Resize onPress={() => {
          navigation.goBack()
        }}
        />
      </ImageBackground>
    </View>
  )
}
