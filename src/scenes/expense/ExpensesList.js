import * as React from 'react'
import {
  Box, FlatList, HStack, Text,
} from 'native-base'
import PropTypes from 'prop-types'
import { TouchableOpacity } from 'react-native'
import { useNavigation } from '@react-navigation/native'
import { currencyFormat } from '../../i18n/language'

const Item = ({ item, onPress }) => (
  <TouchableOpacity onPress={onPress}>
    <HStack space={2} justifyContent="space-between" alignItems="center" w="100%">
      <Box h="10" w="60%">
        <Text
          fontSize="sm"
          _dark={{
            color: 'warmGray.50',
          }}
          color="light.50"
        >
          {item.title.length > 25 ? `${item.title.substr(0, 22)}...` : item.title}
        </Text>
      </Box>
      <Box h="10" w="10%">
        <Text
          _dark={{
            color: 'warmGray.50',
          }}
          color="light.50"
          fontSize="sm"
        >
          {item.quantity}
        </Text>
      </Box>
      <Box h="10" w="20%" alignItems="flex-end">
        <Text
          _dark={{
            color: 'warmGray.50',
          }}
          color="light.50"
          fontSize="sm"
        >
          {currencyFormat.format(item.unitCost)}
        </Text>
      </Box>
    </HStack>
  </TouchableOpacity>
)
export default function ExpensesList({ data }) {
  const navigation = useNavigation()
  return (
    <FlatList
      px="2"
      mb="2"
      data={data}
      renderItem={({ item }) => (
        <Box
          borderBottomWidth="1"
          _dark={{
            borderColor: 'gray.600',
          }}
          borderColor="coolGray.200"
        >
          <Item
            onPress={() => navigation.navigate('ExpenseDetails', { expense: item })}
            item={item}
          />
        </Box>
      )}
      keyExtractor={(item) => item.id}
    />
  )
}

ExpensesList.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.string.isRequired,
    category: PropTypes.shape().isRequired,
    quantity: PropTypes.number.isRequired,
    unitCost: PropTypes.number.isRequired,
  })),
}
ExpensesList.defaultProps = {
  data: [],
}
Item.propTypes = {
  item: PropTypes.shape(
    {
      id: PropTypes.number,
      category: PropTypes.shape(),
      title: PropTypes.string,
      quantity: PropTypes.number,
      unitCost: PropTypes.number,
    },
  ),
  onPress: PropTypes.func,
}
Item.defaultProps = {
  item: null,
  onPress: () => null,
}
