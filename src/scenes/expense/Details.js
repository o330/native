import {
  Button, Divider, HStack, KeyboardAvoidingView, Stack, Text, useToast, View,
} from 'native-base'
import * as React from 'react'
import {
  useEffect, useLayoutEffect, useRef, useState,
} from 'react'
import { useFocusEffect } from '@react-navigation/native'
import { useTranslation } from 'react-i18next'
import { BackHandler } from 'react-native'
import { NumericInput, GeneralInput } from '../../components'
import CategoryDropBox from './category/CategoryDropBox'
import { Submit } from './buttons'
import { useAuthContext } from '../../context/auth'
import { ConnectionIssueToast as showConnectionIssueToast } from '../../components/error'
import { useBillsService } from '../../services/BillsProvider'

/* eslint-disable react/prop-types */
const ExpenseDetails = ({ route, navigation }) => {
  const { t } = useTranslation()
  const [isDisabled, setIsDisabled] = useState(true)
  const [submit, setSubmit] = useState(false)
  const { userToken } = useAuthContext()
  const { updateExpense } = useBillsService()
  const [expense, setExpense] = useState(route?.params?.expense)
  const abortController = new AbortController()
  const toast = useToast()
  const inputEl = useRef(null)

  const onSubmit = () => {
    setSubmit(true)
    updateExpense(expense, userToken, abortController.signal)
      .then(() => {
        navigation.navigate('Home')
      })
      .catch(() => {
        showConnectionIssueToast(toast)
      })
      .then(() => setSubmit(false))
  }
  const handleChange = (event) => {
    const { name, text } = event
    let newValue = text
    if (name === 'category') {
      newValue = {
        id: text,
      }
    }
    setExpense((prevState) => ({
      ...prevState,
      [name]: newValue,
    }))
  }
  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <Button
          pr="5"
          onPress={() => setIsDisabled(false)}
        >{t('edit')}
        </Button>
      ),
    })
  }, [navigation])
  useEffect(() => {
    if (!isDisabled) {
      inputEl.current.focus()
    }
  }, [isDisabled])
  useFocusEffect(
    React.useCallback(() => {
      const onBackPress = () => {
        if (!isDisabled) {
          setExpense(route?.params?.expense)
          setIsDisabled(true)
          return true
        }
        return false
      }

      BackHandler.addEventListener('hardwareBackPress', onBackPress)

      return () => BackHandler.removeEventListener('hardwareBackPress', onBackPress)
    }, [isDisabled]),
  )
  useEffect(() => () => abortController.abort(), [])

  return (
    <View flex={1}>
      <KeyboardAvoidingView flex="1" justifyContent="flex-start" alignItems="flex-start">
        <Stack
          space={2}
          alignSelf="flex-start"
          px="4"
          safeArea
          mt="4"
          w={{
            base: '100%',
            md: '25%',
          }}
        >
          <Text fontSize="xl">{t('purchase')}: </Text>
          <GeneralInput
            inputElementRef={inputEl}
            value={expense?.title}
            onChange={handleChange}
            name="title"
            height={35}
            isDisabled={isDisabled}
          />
          <Text fontSize="xl">{t('category.category')}: </Text>
          <CategoryDropBox
            selectedValue={expense?.category?.id}
            onValueChange={(value) => handleChange({ text: value, name: 'category' })}
            isDisabled={isDisabled}
          />
          <Divider />
          <HStack
            space={1}
            justifyContent="space-between"
            alignItems="flex-start"
          >
            <NumericInput
              flex={1}
              value={expense.quantity.toString()}
              setValue={setExpense}
              name="quantity"
              isDisabled={isDisabled}
            />
            <NumericInput
              flex={1}
              value={expense.unitCost.toString()}
              setValue={setExpense}
              name="unitCost"
              isDisabled={isDisabled}
              InputRightElement={<Text fontSize="sm">zł</Text>}
            />
          </HStack>
          <HStack
            alignItems="center"
            justifyContent="flex-start"
          >
            <Text fontSize="xl">{t('total')}: </Text>
            <Text>{(expense.unitCost * expense.quantity).toString()} zł </Text>
          </HStack>
          {!isDisabled && <Submit onPress={onSubmit} isLoading={submit} />}
        </Stack>
      </KeyboardAvoidingView>
    </View>
  )
}
export default ExpenseDetails
