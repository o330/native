import React from 'react'
import { CheckIcon, Select, VStack } from 'native-base'

export default function CategoryColorSelect({
  colors, selectedValue, onValueChane, ...props
}) {
  return (
    <VStack alignItems="center" space={4}>
      <Select
        selectedValue={selectedValue}
        minWidth="50"
        accessibilityLabel="Choose color"
        bg={`${selectedValue}.300`}
        _selectedItem={{
          endIcon: <CheckIcon size="5" />,
        }}
        mt={1}
        onValueChange={onValueChane}
        variant="rounded"
        {...props}
      >
        {colors && colors.map((value) => <Select.Item label="" value={value} bg={`${value}.300`} />)}
      </Select>
    </VStack>
  )
}
