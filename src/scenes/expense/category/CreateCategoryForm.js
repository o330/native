import React, { useEffect, useReducer } from 'react'
import { LogBox } from 'react-native'
import { View } from 'native-base'
import useCategories from '../../../context/category/CategoriesContext'
import { useAuthContext } from '../../../context/auth'
import CategoriesView from './CategoriesView'
import CategoryReducer from './CategoryReducer'

LogBox.ignoreLogs(['The contrast ratio of 1:1 for darkText on transparent'])

export default function CreateCategoryForm() {
  const { colors, createCategories, fetchDefaultCategories } = useCategories()
  const { userToken } = useAuthContext()
  const controller = new AbortController()
  const removeCategory = (state, action) => {
    const filtered = state.categoriesData.filter((value, idx) => action.index !== idx)
    return {
      ...state,
      categoriesData: filtered,
    }
  }
  const [reducer, initFunc] = CategoryReducer({ removeCategory, initFunc: ({ categories }) => ({ categoriesData: categories }) })
  const [state, dispatch] = useReducer(reducer, { categories: [] }, initFunc)
  const submit = () => {
    createCategories(state.categoriesData.map((item) => item.category), userToken, controller.signal)
  }
  useEffect(() => {
    fetchDefaultCategories(userToken, controller.signal).then((response) => {
      const names = response.data
      const categories = names.map((value, index) => ({
        category: {
          name: value,
          colorScheme: colors[index % colors.length],
        },
        value: (index + 1) * 10,
      }))
      dispatch({ type: 'RESET', categories })
    })
    return () => controller.abort()
  }, [])
  if (state?.categoriesData) {
    return (
      <CategoriesView
        categories={state.categoriesData}
        addRow={() => {
          dispatch({ type: 'ADD_CATEGORY', colors })
        }}
        onDelete={(index) => {
          dispatch({ type: 'REMOVE_CATEGORY', index })
        }}
        onChange={(category, index) => {
          dispatch({ type: 'MODIFY_CATEGORY', category, index })
        }}
        onSubmit={submit}
        topText="createCategoryForm.tip"
      />
    )
  }
  return <View flex={1} />
}
