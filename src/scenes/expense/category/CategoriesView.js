import React, { useRef, useState } from 'react'
import {
  Button, ScrollView, Text, useTheme,
} from 'native-base'
import { useTranslation } from 'react-i18next'
import PropTypes from 'prop-types'
import { Submit } from '../buttons'
import { mapToChartData } from '../../stats/StatsUtil'
import CollapsibleBarChart from '../../stats/CollapsibleBarChart'
import CategoryRow from './CategoryRow'
import { KeyboardAvoidingView } from '../../../components'

export default function CategoriesView({
  categories, addRow, topText, onSubmit, onDelete, onChange,
}) {
  const { t } = useTranslation()
  const theme = useTheme()
  const [showPreview, setShowPreview] = useState()
  const [height, setHeight] = useState(0)
  const ref = useRef(null)

  return (
    <KeyboardAvoidingView flex={1}>
      <Text fontSize="sm">{t(topText)}</Text>
      <CollapsibleBarChart renderData={mapToChartData(categories, theme)} show={showPreview} setShow={setShowPreview} />
      <ScrollView
        ref={ref}
        onContentSizeChange={(w, h) => {
          if (height < h) ref.current.scrollToEnd({ animated: true })
          setHeight(h)
        }}
      >
        {categories && categories.map((item, index) => (
          <CategoryRow
            key={index}
            item={item}
            index={index}
            onChange={onChange}
            onPress={onDelete}
            onFocus={() => setShowPreview(false)}
          />
        ))}
      </ScrollView>
      <Button.Group space={2} mt={2} justifyContent="space-evenly">
        <Button alignSelf="center" onPress={addRow}>{t('addRow')}</Button>
        <Submit
          alignSelf="flex-end"
          onPress={onSubmit}
        />
      </Button.Group>
    </KeyboardAvoidingView>
  )
}

CategoriesView.propTypes = {
  categories: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  topText: PropTypes.string,
  onSubmit: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  addRow: PropTypes.func.isRequired,
}
CategoriesView.defaultProps = {
  topText: '',
}
