import { Box, HStack, Input } from 'native-base'
import React from 'react'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'
import useCategories from '../../../context/category/CategoriesContext'
import { RemoveButton } from '../../../components'
import CategoryColorSelect from './CategoryColorSelect'

export default function CategoryRow({
  item, index, onChange, onPress, onFocus,
}) {
  const { t } = useTranslation()
  const { colors } = useCategories()
  const { category } = item
  const handleChange = (text, name) => {
    onChange({ ...item, category: { ...category, [name]: text } }, index)
  }
  const removeRow = () => {
    onPress(index)
  }
  return (
    <Box>
      <RemoveButton onPress={removeRow} />
      <HStack mx="5" w="90%" space={5}>
        <Input
          flex={5}
          value={category.name}
          onChangeText={(text) => (handleChange(text, 'name'))}
          placeholder={t('category.category')}
          onFocus={onFocus}
        />
        <CategoryColorSelect
          colors={colors}
          selectedValue={category.colorScheme}
          onValueChane={(itemValue) => handleChange(itemValue, 'colorScheme')}
          flex={1}
        />
      </HStack>
    </Box>
  )
}

CategoryRow.propTypes = {
  item: PropTypes.shape({
    category:
      {
        name: PropTypes.string.isRequired,
        colorScheme: PropTypes.string.isRequired,
      },
  }).isRequired,
  index: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
  onPress: PropTypes.func.isRequired,
  onFocus: PropTypes.func,
}
CategoryRow.defaultProps = {
  onFocus: () => null,
}
