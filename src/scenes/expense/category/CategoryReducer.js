export default function categoryReducer(props) {
  const defaultAddCategory = (state, action) => {
    const categories = state.categoriesData
    const { colors } = action
    return {
      ...state,
      categoriesData: [...categories, {
        category: {
          name: '',
          colorScheme: colors[categories.length % colors.length],
        },
        value: (categories.length + 1) * 10,
      }],
      modified: true,
    }
  }
  const defaultRemoveCategory = (state) => {
    const filtered = state.categoriesData.filter((value, idx) => state.deletionIndex !== idx)
    return {
      ...state,
      showModal: false,
      deletionIndex: null,
      categoriesData: filtered,
      modified: true,
    }
  }
  const defaultHandleCategoryChange = (state, action) => {
    const newCategories = state.categoriesData
    newCategories[action.index] = action.category
    return {
      ...state,
      modified: true,
      categoriesData: newCategories,
    }
  }
  const defaultInitFunc = ({ categories }) => {
    const categoriesData = categories.map((item, index) => ({ category: item, value: (index + 1) * 10 }))
    return {
      showModal: false,
      modified: false,
      categoriesData,
    }
  }
  const addCategory = props?.addCategory || defaultAddCategory
  const removeCategory = props?.removeCategory || defaultRemoveCategory
  const handleCategoryChange = props?.handleCategoryChange || defaultHandleCategoryChange
  const initFunc = props?.initFunc || defaultInitFunc

  const reducer = (state, action) => {
    switch (action.type) {
      case 'SHOW_CONFIRMATION_MODAL':
        return {
          ...state,
          showModal: true,
          deletionIndex: action.index,
        }
      case 'CANCEL_DELETION':
        return {
          ...state,
          showModal: false,
          deletionIndex: null,
        }
      case 'REMOVE_CATEGORY':
        return removeCategory(state, action)
      case 'ADD_CATEGORY':
        return addCategory(state, action)
      case 'MODIFY_CATEGORY':
        return handleCategoryChange(state, action)
      case 'RESET':
        return initFunc(action)
      default:
        throw new Error()
    }
  }
  return [reducer, initFunc]
}
