import React from 'react'
import { CheckIcon, Select } from 'native-base'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'
import useCategories from '../../../context/category/CategoriesContext'

const addUncategorizedOption = (categories, additionalCategories) => {
  if (additionalCategories) {
    return additionalCategories.concat(categories)
  }
  return categories
}

function CategoryDropBox({
  onValueChange, selectedValue, isDisabled, additionalCategories,
}) {
  const { t } = useTranslation()
  const categories = addUncategorizedOption(useCategories().categories, additionalCategories)
  return (
    <Select
      selectedValue={selectedValue}
      variant="rounded"
      minWidth="200"
      accessibilityLabel={t('category.category')}
      placeholder={t('category.category')}
      dropdownIcon=""
      _selectedItem={{
        bg: 'teal.600',
        endIcon: <CheckIcon size={5} />,
      }}
      mt="1"
      onValueChange={onValueChange}
      isDisabled={isDisabled}
    >
      {categories && categories.map((category) => (
        <Select.Item value={category.id} label={category.name} key={category.id} />))}
    </Select>
  )
}

export default ({
  // eslint-disable-next-line react/prop-types
  onValueChange, selectedValue, isDisabled, additionalCategories,
}) => (
  <CategoryDropBox
    onValueChange={onValueChange}
    selectedValue={selectedValue}
    isDisabled={isDisabled}
    additionalCategories={additionalCategories}
  />
)

CategoryDropBox.propTypes = {
  onValueChange: PropTypes.func,
  selectedValue: PropTypes.number,
  isDisabled: PropTypes.bool,
  additionalCategories: PropTypes.arrayOf(PropTypes.shape()),
}
CategoryDropBox.defaultProps = {
  onValueChange: () => null,
  selectedValue: null,
  isDisabled: false,
  additionalCategories: null,
}
