import React, { useReducer, useState } from 'react'
import { BackHandler, LogBox } from 'react-native'
import { View } from 'native-base'
import { useFocusEffect } from '@react-navigation/native'
import { useTranslation } from 'react-i18next'
import PropTypes from 'prop-types'
import useCategories from '../../../context/category/CategoriesContext'
import CategoriesView from './CategoriesView'
import { useAuthContext } from '../../../context/auth'
import { ApprovalModal } from '../../../components'
import CategoryReducer from './CategoryReducer'

LogBox.ignoreLogs(['The contrast ratio of 1:1 for darkText on transparent'])

export default function EditCategoriesForm({ navigation }) {
  const { t } = useTranslation()
  const {
    categories, updateCategories, colors, refreshContext,
  } = useCategories()
  const [reducer, initFunc] = CategoryReducer()
  const [state, dispatch] = useReducer(reducer, { categories }, initFunc)
  const [quit, setQuit] = useState(false)
  const { userToken } = useAuthContext()
  const controller = new AbortController()
  const submit = () => {
    updateCategories(state.categoriesData.map((item) => item.category), userToken, controller.signal)
      .then(() => {
        refreshContext()
        navigation.navigate('HomeTabNavigator')
      })
  }
  useFocusEffect(
    React.useCallback(() => {
      const onBackPress = () => {
        if (state.modified) {
          setQuit(!quit)
          return true
        }
        return false
      }
      BackHandler.addEventListener('hardwareBackPress', onBackPress)

      return () => BackHandler.removeEventListener('hardwareBackPress', onBackPress)
    }, [quit, state.modified]),
  )

  return (
    <View flex={1}>
      <ApprovalModal
        modalVisible={state.showModal}
        onApprove={() => dispatch({ type: 'REMOVE_CATEGORY' })}
        onCancel={() => dispatch({ type: 'CANCEL_DELETION' })}
        header={t('editCategoriesForm.deletionModalHeader')}
        text={t('editCategoriesForm.deletionModalText')}

      />
      <ApprovalModal
        modalVisible={quit}
        onApprove={() => navigation.navigate('HomeTabNavigator')}
        onCancel={() => setQuit(false)}
        header={t('editCategoriesForm.quitModalHeader')}
        text={t('editCategoriesForm.quitModalText')}
      />
      <CategoriesView
        categories={state.categoriesData}
        addRow={() => {
          dispatch({ type: 'ADD_CATEGORY', colors })
        }}
        onDelete={(index) => {
          dispatch({ type: 'SHOW_CONFIRMATION_MODAL', index })
        }}
        onChange={(category, index) => {
          dispatch({ type: 'MODIFY_CATEGORY', category, index })
        }}
        onSubmit={submit}
      />
    </View>
  )
}
EditCategoriesForm.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }),
}

EditCategoriesForm.defaultProps = {
  navigation: { navigate: () => null },
}
