import React from 'react'
import { Icon, IconButton } from 'native-base'
import { Entypo } from '@expo/vector-icons'
import PropTypes from 'prop-types'

export default function DeleteButton({ onPress, ...rest }) {
  return (
    <IconButton
      onPress={onPress}
      {...rest}
      icon={<Icon as={Entypo} name="trash" />}
      borderRadius="full"
      _icon={{
        color: 'darkgray',
        size: '15px',
      }}
      _hover={{
        bg: 'red.600:alpha.20',
      }}
      _pressed={{
        bg: 'red.600:alpha.20',
        _icon: {
          name: 'trash',
        },
        _ios: {
          _icon: {
            size: '6',
          },
        },
      }}
      _ios={{
        _icon: {
          size: '2xl',
        },
      }}
    />
  )
}
DeleteButton.propTypes = {
  onPress: PropTypes.func,
}

DeleteButton.defaultProps = {
  onPress: () => null,
}
