import React from 'react'
import { Icon, IconButton } from 'native-base'
import { Entypo } from '@expo/vector-icons'
import PropTypes from 'prop-types'

export default function AddButton({ navigation }) {
  return (
    <IconButton
      onPress={() => navigation.navigate('BillForm')}
      icon={<Icon as={Entypo} name="circle-with-plus" />}
      borderRadius="full"
      alignSelf="flex-end"
      _icon={{
        color: 'cyan.500',
        size: 'xl',
      }}
      _hover={{
        bg: 'blue.600:alpha.20',
      }}
      _pressed={{
        bg: 'darkblue.600:alpha.20',
        _icon: {
          name: 'circle-with-plus',
        },
        _ios: {
          _icon: {
            size: 'lg',
          },
        },
      }}
      _ios={{
        _icon: {
          size: '2xl',
        },
      }}
    />
  )
}

AddButton.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }),
}

AddButton.defaultProps = {
  navigation: {
    navigate: () => null,
  },
}
