import React from 'react'
import { Button } from 'native-base'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'

export default function Submit({ onPress, isLoading, ...rest }) {
  const { t } = useTranslation()
  return (
    <Button
      isLoading={isLoading}
      _loading={{
        bg: 'amber.400:alpha.70',
        _text: {
          color: 'coolGray.700',
        },
      }}
      _spinner={{
        color: 'white',
      }}
      isLoadingText={t('submit')}
      onPress={onPress}
      {...rest}
    >
      {t('submit')}
    </Button>
  )
}

Submit.propTypes = {
  onPress: PropTypes.func,
  isLoading: PropTypes.bool,
}

Submit.defaultProps = {
  onPress: () => null,
  isLoading: false,

}
