import * as React from 'react'
import { useContext } from 'react'
import PropTypes from 'prop-types'
import axios from 'axios'

const colors = ['rose', 'pink', 'fuchsia', 'purple', 'violet', 'indigo', 'blue', 'lightBlue', 'cyan', 'teal', 'emerald',
  'green', 'lime', 'yellow', 'amber', 'orange', 'red', 'warmGray']

const CategoriesContext = React.createContext(null)

export const defaultFetchDefaultCategories = (authorization, signal) => axios.get('/api/categories/defaults', {
  signal,
  headers: { Authorization: authorization },
})
export const defaultGetCategories = (authorization, signal) => axios.get('/api/categories/', {
  signal,
  headers: { Authorization: authorization },
})

export const CategoriesContextProvider = ({
  categories,
  fetchDefaultCategories,
  getCategories,
  createCategories,
  updateCategories,
  dispatch,
  children,
}) => {
  const defaultCreateCategory = (category, authorization, signal) => axios.post('/api/categories/', category, {
    signal,
    headers: { Authorization: authorization },
  }).then(() => {
    dispatch({ type: 'REFRESH' })
  })
  const defaultUpdateCategories = (category, authorization, signal) => axios.put('/api/categories/', category, {
    signal,
    headers: { Authorization: authorization },
  })
  const refreshContext = () => {
    dispatch({ type: 'REFRESH' })
  }
  const value = {
    categories,
    fetchDefaultCategories,
    getCategories,
    refreshContext,
    createCategories: createCategories || defaultCreateCategory,
    updateCategories: updateCategories || defaultUpdateCategories,
    colors,
  }
  return (
    <CategoriesContext.Provider value={value}>
      {children}
    </CategoriesContext.Provider>
  )
}
export default function useCategories() {
  return useContext(CategoriesContext)
}

CategoriesContextProvider.propTypes = {
  categories: PropTypes.arrayOf(PropTypes.shape(
    {
      id: PropTypes.number,
      name: PropTypes.string,
      colorScheme: PropTypes.string,
    },
  )),
  createCategories: PropTypes.func,
  updateCategories: PropTypes.func,
  dispatch: PropTypes.func.isRequired,
  fetchDefaultCategories: PropTypes.func,
  getCategories: PropTypes.func,
}
CategoriesContextProvider.defaultProps = {
  categories: [],
  fetchDefaultCategories: defaultFetchDefaultCategories,
  createCategories: null,
  updateCategories: null,
  getCategories: defaultGetCategories,
}
