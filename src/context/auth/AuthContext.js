import * as React from 'react'
import { useContext } from 'react'
import axios from 'axios'
import PropTypes from 'prop-types'
import * as SecureStore from 'expo-secure-store'
import { ConnectionIssueToast as showConnectionIssueToast } from '../../components/error'

const AuthContext = React.createContext(null)
async function save(key, value) {
  await SecureStore.setItemAsync(key, value)
}

function authenticated(username, dispatch) {
  return async (response) => {
    const { authorization } = response.headers
    await save('authToken', authorization)
    await save('username', username)
    dispatch({ type: 'SIGN_IN', token: authorization, username })
  }
}

const defaultSignIn = async ({
  dispatch,
  username,
  password,
  setIsInvalid,
  toast,
}) => {
  await axios.post('/api/auth/login', { username, password })
    .then(authenticated(username, dispatch))
    .catch((error) => {
      if (error?.response?.status === 401 && setIsInvalid) {
        setIsInvalid(true)
      } else showConnectionIssueToast(toast)
    })
}
const defaultSignUp = async ({
  dispatch, username, password, toast,
}) => {
  await axios.post('/api/user/add', { username, password })
    .then(() => {
      defaultSignIn({
        dispatch, username, password, toast,
      })
    })
    .catch((() => {
      showConnectionIssueToast(toast)
    }))
}
const defaultSignOut = async (dispatch) => {
  await SecureStore.deleteItemAsync('authToken')
  await SecureStore.deleteItemAsync('username')
  dispatch({ type: 'SIGN_OUT' })
}

export const AuthContextProvider = ({
  isLoading,
  isSignout,
  userToken,
  dispatch,
  username,
  signIn,
  signOut,
  signUp,
  children,
}) => {
  const value = {
    isLoading,
    isSignout,
    userToken,
    username,
    dispatch,
    signIn,
    signOut,
    signUp,
  }
  return (
    <AuthContext.Provider value={value}>
      {children}
    </AuthContext.Provider>
  )
}
export default function useAuthContext() {
  return useContext(AuthContext)
}

AuthContextProvider.propTypes = {
  isLoading: PropTypes.bool,
  isSignout: PropTypes.bool,
  userToken: PropTypes.string,
  username: PropTypes.string,
  dispatch: PropTypes.func.isRequired,
  signIn: PropTypes.func,
  signOut: PropTypes.func,
  signUp: PropTypes.func,
}
AuthContextProvider.defaultProps = {
  isLoading: true,
  isSignout: false,
  userToken: null,
  username: null,
  signIn: defaultSignIn,
  signOut: defaultSignOut,
  signUp: defaultSignUp,
}
