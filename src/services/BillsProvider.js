import axios from 'axios'
import React from 'react'
import PropTypes from 'prop-types'

const { createContext, useContext } = React
const BillContext = createContext(null)

export const defaultGetBills = (params, authorization, signal) => axios.get('/bills/', {
  params,
  signal,
  headers: { Authorization: authorization },
})

const defaultCreateBill = (bill, authorization, signal) => axios.post('/bills/', bill, {
  signal,
  headers: { Authorization: authorization },
})

const defaultUpdateBill = (bill, authorization, signal) => axios.put('/bills/', bill, {
  signal,
  headers: { Authorization: authorization },
})

const defaultDeleteBill = (id, authorization, signal) => axios.delete(`/bills/${id}`, {
  signal,
  headers: { Authorization: authorization },
})

const defaultUpdateExpense = (bill, authorization, signal) => axios.put('/bills/expense', bill, {
  signal,
  headers: { Authorization: authorization },
})

export const BillsProvider = ({
  children,
  createBill,
  updateBill,
  deleteBill,
  getBills,
  updateExpense,
}) => {
  const value = {
    getBills,
    createBill,
    updateBill,
    deleteBill,
    updateExpense,
  }
  return (
    <BillContext.Provider value={value}>
      {children}
    </BillContext.Provider>
  )
}
export const useBillsService = () => useContext(BillContext)

BillsProvider.propTypes = {
  createBill: PropTypes.func,
  updateBill: PropTypes.func,
  deleteBill: PropTypes.func,
  getBills: PropTypes.func,
  updateExpense: PropTypes.func,
}
BillsProvider.defaultProps = {
  getBills: defaultGetBills,
  createBill: defaultCreateBill,
  updateBill: defaultUpdateBill,
  deleteBill: defaultDeleteBill,
  updateExpense: defaultUpdateExpense,
}
