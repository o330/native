import axios from 'axios'
import React from 'react'
import PropTypes from 'prop-types'

const { createContext, useContext } = React
const ExpensesStatsContext = createContext(null)

function get(url, params, authorization, signal) {
  return axios.get(url, {
    params,
    signal,
    headers: { Authorization: authorization },
  })
}

const defaultGetTotalSpending = (params, authorization, signal) => get('/stats/total', params, authorization, signal)
const defaultGetPeriodSpending = (params, authorization, signal) => get('/stats/period', params, authorization, signal)

const defaultGetMonthParams = () => {
  const date = new Date()
  const from = new Date(date.getFullYear(), date.getMonth(), 1)
  const to = new Date()
  return { from, to }
}
function getMonday(date) {
  const day = date.getDay()
  const diff = date.getDate() - day + (day === 0 ? -6 : 1) // adjust when day is sunday
  return new Date(date.setDate(diff))
}
const defaultGetWeekParams = () => {
  const from = getMonday(new Date())
  const to = new Date()
  return { from, to }
}
export const ExpensesStatsProvider = ({
  children,
  getTotalSpending,
  getPeriodSpending,
  getMonthParams,
  getWeekParams,
}) => {
  const value = {
    getTotalSpending,
    getPeriodSpending,
    getMonthParams,
    getWeekParams,
  }
  return (
    <ExpensesStatsContext.Provider value={value}>
      {children}
    </ExpensesStatsContext.Provider>
  )
}
export const useExpensesStats = () => useContext(ExpensesStatsContext)

ExpensesStatsProvider.propTypes = {
  getTotalSpending: PropTypes.func,
  getPeriodSpending: PropTypes.func,
  getMonthParams: PropTypes.func,
  getWeekParams: PropTypes.func,
}
ExpensesStatsProvider.defaultProps = {
  getTotalSpending: defaultGetTotalSpending,
  getPeriodSpending: defaultGetPeriodSpending,
  getMonthParams: defaultGetMonthParams,
  getWeekParams: defaultGetWeekParams,
}
