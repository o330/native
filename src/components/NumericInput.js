import React from 'react'
import PropTypes from 'prop-types'
import GeneralInput from './GeneralInput'

function isNumeric(n) {
  const isNumber = !isNaN(parseFloat(n)) && isFinite(n)
  const s = n.toString().split('.')
  const hasMaxTwoDecimals = s.length < 2 || s[1].length < 3
  return isNumber && hasMaxTwoDecimals
}
const handleChange = (event) => {
  const { name, text, setValue } = event
  if (isNumeric(text) || (!text)) {
    setValue((prevState) => ({
      ...prevState,
      [name]: text,
    }))
  } else {
    setValue((prevState) => ({ ...prevState }))
  }
}
export default function NumericInput({
  value, setValue, name, editable, ref, ...props
}) {
  return (
    <GeneralInput
      defaultValue={value}
      value={value}
      onChange={handleChange}
      name={name}
      setValue={setValue}
      keyboardType="numeric"
      variant="unstyled"
      textAlign="right"
      maxLength={9}
      inputElementRef={ref}
      {...props}
    />
  )
}
NumericInput.propTypes = {
  value: PropTypes.string,
  name: PropTypes.string,
  setValue: PropTypes.func,
  editable: PropTypes.bool,
  ref: PropTypes.shape(),
}
NumericInput.defaultProps = {
  value: null,
  name: null,
  setValue: () => null,
  editable: true,
  ref: null,
}
