import { Icon, IconButton } from 'native-base'
import { Entypo } from '@expo/vector-icons'
import React from 'react'

export default function Resize(props) {
  return (
    <IconButton
      {...props}
      icon={<Icon as={Entypo} name="resize-100-" />}
      borderRadius="full"
      alignSelf="flex-end"
      _icon={{
        bg: 'warmGray:alpha.10',
        color: 'white',
        size: 'lg',
      }}
      _hover={{
        bg: 'warmGray.700:alpha.20',
      }}
      _pressed={{
        bg: 'warmGray.700:alpha.20',
        _icon: {
          name: 'resize-100-',
        },
        _ios: {
          _icon: {
            size: 'lg',
          },
        },
      }}
      _ios={{
        _icon: {
          size: '2xl',
        },
      }}
    />
  )
}
