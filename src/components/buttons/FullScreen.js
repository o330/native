import { Icon, IconButton } from 'native-base'
import { Entypo } from '@expo/vector-icons'
import React from 'react'

export default function FullScreen(props) {
  return (
    <IconButton
      {...props}
      icon={<Icon as={Entypo} name="resize-full-screen" />}
      borderRadius="full"
      alignSelf="flex-end"
      _icon={{
        bg: 'white:alpha.20',
        color: 'warmGray.900',
        size: 'sm',
      }}
      _hover={{
        bg: 'warmGray.700:alpha.20',
      }}
      _pressed={{
        bg: 'warmGray.700:alpha.20',
        _icon: {
          name: 'resize-full-screen',
        },
        _ios: {
          _icon: {
            size: 'lg',
          },
        },
      }}
      _ios={{
        _icon: {
          size: '2xl',
        },
      }}
    />
  )
}
