import * as React from 'react'
import { useState } from 'react'
import {
  HStack, Icon, IconButton, Text,
} from 'native-base'
import { Platform } from 'react-native'
import { Entypo } from '@expo/vector-icons'
import DateTimePicker from '@react-native-community/datetimepicker'
import PropTypes from 'prop-types'

export default function DatePicker({ label, date, setDate }) {
  const [show, setShow] = useState(false)
  const showDatepicker = () => {
    setShow(true)
  }
  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date
    setShow(Platform.OS === 'ios')
    setDate(currentDate)
  }

  return (
    <HStack>
      {label && <Text bold>{label}</Text>}
      <IconButton
        onPress={showDatepicker}
        icon={(
          <Icon as={Entypo} name="calendar" />)}
        _icon={{
          color: 'darkgray',
          size: 'sm',
        }}
      />
      { show && (
      <DateTimePicker
        testID="dateTimePicker"
        value={date}
        mode="date"
        is24Hour
        display="default"
        onChange={onChange}
      />
      ) }
    </HStack>
  )
}
DatePicker.propTypes = {
  date: PropTypes.shape({
    toLocaleDateString: PropTypes.func,
  }),
  setDate: PropTypes.func,
  label: PropTypes.string,
}

DatePicker.defaultProps = {
  date: new Date(),
  label: null,
  setDate: () => null,
}
