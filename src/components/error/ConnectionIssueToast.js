export default function (toast) {
  return (toast.show({
    title: 'Something went wrong',
    status: 'error',
    description: 'It looks like there is an issue with internet connection!',
    placement: 'top',
  }))
}
