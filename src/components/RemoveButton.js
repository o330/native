import { Icon, IconButton } from 'native-base'
import { Entypo } from '@expo/vector-icons'
import React from 'react'

export default function RemoveButton(props) {
  return (
    <IconButton
      {...props}
      icon={<Icon as={Entypo} name="cross" />}
      borderRadius="full"
      alignSelf="flex-end"
      _icon={{
        color: 'rose.500',
        size: '4',
      }}
      _hover={{
        bg: 'rose.600:alpha.20',
      }}
      _pressed={{
        bg: 'rose.800:alpha.20',
        _icon: {
          name: 'cross',
        },
        _ios: {
          _icon: {
            size: 'lg',
          },
        },
      }}
      _ios={{
        _icon: {
          size: '2xl',
        },
      }}
    />
  )
}
