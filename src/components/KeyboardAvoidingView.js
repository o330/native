import React, { useEffect, useState } from 'react'
import { View } from 'native-base'
import { Keyboard } from 'react-native'

export default function KeyboardAvoidingView(props) {
  const [pb, setPb] = useState(0)

  useEffect(() => {
    const showSubscription = Keyboard.addListener('keyboardDidShow', (event) => {
      setPb(event.endCoordinates.height * 1.2)
    })
    const hideSubscription = Keyboard.addListener('keyboardDidHide', () => {
      setPb(0)
    })
    return () => {
      showSubscription.remove()
      hideSubscription.remove()
    }
  }, [])
  return (
    <View pb={pb} {...props} />
  )
}
