import React from 'react'
import { Button, Modal, Text } from 'native-base'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'

export default function ApprovalModal({
  modalVisible, onApprove, onCancel, text, header,
}) {
  const { t } = useTranslation()
  return (
    <Modal isOpen={modalVisible} onClose={onCancel} size="full">
      <Modal.Content maxH="212">
        <Modal.CloseButton />
        <Modal.Header>{header}</Modal.Header>
        <Modal.Body>
          <Text fontSize="sm">{text}</Text>
        </Modal.Body>
        <Modal.Footer>
          <Button.Group space={2}>
            <Button
              variant="ghost"
              colorScheme="blueGray"
              onPress={onCancel}
            >
              {t('approvalModal.cancel')}
            </Button>
            <Button
              onPress={onApprove}
            >
              {t('approvalModal.confirm')}
            </Button>
          </Button.Group>
        </Modal.Footer>
      </Modal.Content>
    </Modal>
  )
}
ApprovalModal.propTypes = {
  modalVisible: PropTypes.bool,
  onApprove: PropTypes.func,
  onCancel: PropTypes.func,
  text: PropTypes.string,
  header: PropTypes.string,
}
ApprovalModal.defaultProps = {
  modalVisible: false,
  onApprove: () => null,
  onCancel: () => null,
  text: '',
  header: '',
}
