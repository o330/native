import React, { useEffect, useState } from 'react'
import * as MediaLibrary from 'expo-media-library'
import { useTranslation } from 'react-i18next'
import * as ImagePicker from 'expo-image-picker'
import {
  Center, Icon, Stack, Text,
} from 'native-base'
import { ImageBackground, TouchableOpacity } from 'react-native'
import { Entypo } from '@expo/vector-icons'
import PropTypes from 'prop-types'
import { useNavigation } from '@react-navigation/native'
import { FullScreen } from './index'

export default function DefaultImagePicker({ uri, onSuccess, options }) {
  const { t } = useTranslation()
  const navigation = useNavigation()
  const [hasPermission, setHasPermission] = useState(null)
  useEffect(() => {
    (async () => {
      const { status } = await MediaLibrary.requestPermissionsAsync()
      setHasPermission(status === 'granted')
    })()
  }, [])
  const pickImage = async () => {
    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      quality: 1,
      ...options,
    })
    if (!result.cancelled) {
      onSuccess(result)
    }
  }
  return (
    <Center>
      {uri && hasPermission ? (
        <TouchableOpacity onPress={pickImage}>
          <ImageBackground key={uri} source={{ uri }} style={{ width: 200, height: 200, alignItems: 'flex-end' }}>
            <FullScreen onPress={() => { navigation.navigate('FullScreenImage', { uri }) }} />
          </ImageBackground>
        </TouchableOpacity>
      ) : (
        <TouchableOpacity onPress={pickImage}>
          <Stack space={5} alignItems="center">
            <Text>{t('imagePicker.header')}</Text>
            <Icon h={200} as={Entypo} name="image" size="5xl" />
          </Stack>
        </TouchableOpacity>
      ) }
    </Center>
  )
}
DefaultImagePicker.propTypes = {
  uri: PropTypes.string,
  onSuccess: PropTypes.func,
  options: PropTypes.shape(),
}

DefaultImagePicker.defaultProps = {
  uri: null,
  onSuccess: () => null,
  options: {},
}
