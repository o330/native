import { Input } from 'native-base'
import React from 'react'
import PropTypes from 'prop-types'

const GeneralInput = ({
  value, name, onChange, inputElementRef, setValue, ...props
}) => (
  <Input
    defaultValue={value}
    ref={inputElementRef}
    value={value}
    onChangeText={(text) => onChange({ name, text, setValue })}
    {...props}
  />
)
export default GeneralInput

GeneralInput.propTypes = {
  value: PropTypes.string,
  setValue: PropTypes.func,
  name: PropTypes.string,
  onChange: PropTypes.func,
  inputElementRef: PropTypes.shape(),
}
GeneralInput.defaultProps = {
  value: null,
  setValue: () => null,
  name: null,
  onChange: () => null,
  inputElementRef: null,
}
