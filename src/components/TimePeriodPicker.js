import { Box, HStack } from 'native-base'
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import PropTypes from 'prop-types'
import ShowDatePicker from './DatePicker'

const props = {
  box: {
    w: '40%',
    _text: {
      textAlign: 'left',
    },
  },
}
export default function TimePeriodPicker({
  startDate, setStartDate, endDate, setEndDate,
}) {
  const { t } = useTranslation()
  return (
    <HStack w="95%" space={1} justifyContent="space-between" alignItems="center">
      <Box
        {...props}
      >
        <ShowDatePicker
          label={startDate ? `${t('from')}: ${startDate?.toLocaleDateString('en-GB')}` : `${t('from')}: -.-.-`}
          date={startDate || new Date()}
          setDate={setStartDate}
        />
      </Box>
      <Box
        {...props}
      >
        <ShowDatePicker
          label={endDate ? `${t('to')}: ${endDate?.toLocaleDateString('en-GB')}` : `${t('to')}: -.-.-`}
          date={endDate || new Date()}
          setDate={setEndDate}
        />
      </Box>
    </HStack>
  )
}
TimePeriodPicker.propTypes = {
  startDate: PropTypes.shape({
    toLocaleDateString: PropTypes.func,
  }),
  endDate: PropTypes.shape({
    toLocaleDateString: PropTypes.func,
  }),
  setStartDate: PropTypes.func,
  setEndDate: PropTypes.func,
}

TimePeriodPicker.defaultProps = {
  startDate: null,
  endDate: null,
  setStartDate: () => null,
  setEndDate: () => null,
}
