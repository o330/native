import { extendTheme } from 'native-base'

// eslint-disable-next-line import/prefer-default-export
export const baseTheme = extendTheme({
  components: {
    Text: {
      defaultProps: {
        fontSize: 'lg',
        color: 'white',
      },
    },
    View: {
      defaultProps: {
        bgColor: '#252525',
      },
    },
    Heading: {
      defaultProps: {
        color: 'white',
      },
    },
  },
  config: {
    // Changing initialColorMode to 'dark'
    initialColorMode: 'dark',
  },
})
