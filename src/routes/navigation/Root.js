import * as React from 'react'
import * as SecureStore from 'expo-secure-store'
import axios from '../../http/Config'
import { AuthContextProvider } from '../../context/auth'
import SignedInStack from './signed-in'
import SignedOutStack from './signed-out'
import LoadingScreen from '../../scenes/splash'

function reducer(state, action) {
  switch (action.type) {
    case 'RESTORE_TOKEN':
      return {
        ...state,
        userToken: action.token,
        username: action.username,
      }
    case 'SIGN_IN':
      return {
        ...state,
        isSignout: false,
        userToken: action.token,
        username: action.username,
      }
    case 'SIGN_OUT':
      return {
        ...state,
        isSignout: true,
        userToken: null,
        username: null,
      }
    case 'LOADED':
      return {
        ...state,
        isLoading: false,
      }
    default:
      throw new Error()
  }
}

export default function Root() {
  const [state, dispatch] = React.useReducer(reducer,
    {
      isLoading: true,
      isSignout: false,
      userToken: null,
      username: null,
    })

  React.useEffect(() => {
    // Fetch the token from storage then navigate to our appropriate place
    const bootstrapAsync = async () => {
      let userToken
      let username

      try {
        userToken = await SecureStore.getItemAsync('authToken')
        username = await SecureStore.getItemAsync('username')
      } catch (e) {
        console.log(e)
      }

      if (userToken) {
        axios.get('/api/auth/', {
          headers: { Authorization: userToken },
        }).then(() => dispatch({ type: 'RESTORE_TOKEN', token: userToken, username }))
          .catch(async () => {
            await SecureStore.deleteItemAsync('authToken')
            await SecureStore.deleteItemAsync('username')
          })
          .then(() => dispatch({ type: 'LOADED' }))
      } else dispatch({ type: 'LOADED' })
    }
    bootstrapAsync()
  }, [])
  if (state.isLoading) return <LoadingScreen />
  return (
    <AuthContextProvider dispatch={dispatch} userToken={state.userToken} username={state.username}>
      {state.userToken == null ? <SignedOutStack /> : <SignedInStack />}
    </AuthContextProvider>
  )
}
