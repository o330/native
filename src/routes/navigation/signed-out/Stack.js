import * as React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { useTranslation } from 'react-i18next'
import { SignInScreen, SignUpForm } from '../../../scenes/login'

const Stack = createStackNavigator()

const style = {
  headerStyle: {
    backgroundColor: '#f4511e',
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
}
const theme = {
  colors: {
    background: '#252525',
  },
}

export default function SignedOutStack() {
  const { t } = useTranslation()
  return (
    <NavigationContainer theme={theme}>
      <Stack.Navigator screenOptions={{ ...style }}>
        <Stack.Screen
          name="SignIn"
          component={SignInScreen}
          options={() => ({ title: t('login.login') })}
        />
        <Stack.Screen
          name="SignUp"
          component={SignUpForm}
          options={() => ({ title: t('login.signup') })}
        />
      </Stack.Navigator>
    </NavigationContainer>
  )
}
