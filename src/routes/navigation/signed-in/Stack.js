import * as React from 'react'
import { useEffect, useReducer } from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { useTranslation } from 'react-i18next'
import PropTypes from 'prop-types'
import DrawerNavigator from './Drawer'
import { useAuthContext } from '../../../context/auth'
import useCategories, { CategoriesContextProvider } from '../../../context/category/CategoriesContext'
import { ExpenseDetails } from '../../../scenes/expense'
import ExpenseStatsScreen from '../../../scenes/stats/ExpenseStatsScreen'
import CreateBillForm from '../../../scenes/bill/form'
import CreateCategoryForm from '../../../scenes/expense/category/CreateCategoryForm'
import LoadingScreen from '../../../scenes/splash'
import FullScreenImage from '../../../scenes/image/FullScreenImage'
import { BillsProvider } from '../../../services/BillsProvider'

const style = {
  headerStyle: {
    backgroundColor: '#f4511e',
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold',
  },
}
const theme = {
  colors: {
    background: '#252525',
  },
}

const Stack = createStackNavigator()

const reducer = (prevState, action) => {
  switch (action.type) {
    case 'REFRESH':
      return {
        ...prevState,
        refresh: true,
      }
    case 'REFRESHED':
      return {
        ...prevState,
        refresh: false,
      }
    case 'UPDATE_CATEGORIES':
      return {
        ...prevState,
        categories: action.categories,
        isLoading: false,
      }
    default:
      throw new Error()
  }
}

export function SignedInStack({ state, dispatch }) {
  const { t } = useTranslation()
  const { userToken } = useAuthContext()
  const { getCategories } = useCategories()

  const controller = new AbortController()
  useEffect(() => {
    if (state.refresh) {
      getCategories(userToken, controller.signal)
        .then((response) => {
          if (response.data) {
            dispatch({ type: 'UPDATE_CATEGORIES', categories: response.data })
          }
        })
      dispatch({ type: 'REFRESHED' })
    }
  }, [state.refresh])
  useEffect(() => () => controller.abort(), [])
  const setUpAccountStack = (
    <Stack.Screen name="CreateCategories" component={CreateCategoryForm} options={() => ({ title: t('createCategoryForm.screenTitle') })} />
  )
  const signedInStack = (
    <>
      <Stack.Screen name="DrawerNavigator" component={DrawerNavigator} options={{ headerShown: false }} />
      <Stack.Screen name="LoadingScreen" component={LoadingScreen} options={{ headerShown: false }} />
      <Stack.Screen name="ExpenseDetails" component={ExpenseDetails} options={() => ({ title: t('expenseDetailsHeading') })} />
      <Stack.Screen name="BillForm" component={CreateBillForm} options={() => ({ title: t('createBillHeading') })} />
      <Stack.Screen name="ExpenseStatsScreen" component={ExpenseStatsScreen} options={() => ({ title: t('stats') })} />
      <Stack.Screen name="FullScreenImage" component={FullScreenImage} options={{ headerShown: false }} />
    </>
  )
  if (state.isLoading) return <LoadingScreen />
  return (
    <NavigationContainer theme={theme}>
      <Stack.Navigator screenOptions={{ ...style }}>
        {state.categories.length === 0 ? setUpAccountStack : signedInStack}
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default () => {
  const [state, dispatch] = useReducer(reducer, {
    refresh: true,
    categories: [],
    isLoading: true,
  })

  return (
    <CategoriesContextProvider categories={state.categories} dispatch={dispatch}>
      <BillsProvider>
        <SignedInStack state={state} dispatch={dispatch} />
      </BillsProvider>
    </CategoriesContextProvider>
  )
}

SignedInStack.propTypes = {
  state: PropTypes.shape({
    refresh: PropTypes.bool,
    categories: PropTypes.arrayOf(PropTypes.shape()),
    isLoading: PropTypes.bool,
  }).isRequired,
}
