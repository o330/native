import React from 'react'
import PropTypes from 'prop-types'
import { Heading, View } from 'native-base'

const DrawerMenu = () => (
  <View
    flex={1}
    safeArea
    alignItems="center"
  >
    <Heading>Menu</Heading>
  </View>
)

DrawerMenu.propTypes = {
  navigation: PropTypes.shape({
    dispatch: PropTypes.func,
  }),
}

DrawerMenu.defaultProps = {
  navigation: {
    dispatch: () => null,
  },
}

export default DrawerMenu
