import React from 'react'
import { Text, useTheme, View } from 'native-base'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import FontIcon from 'react-native-vector-icons/FontAwesome5'
import { useTranslation } from 'react-i18next'
import HomeScreen from '../../../scenes/home'
import Profile from '../../../scenes/profile/ProfileScreen'
import CameraScreen from '../../../scenes/camera/CameraScreen'

const Tab = createBottomTabNavigator()
const renderTabBarLabel = (focused, label) => {
  const color = focused ? 'white' : 'black'
  return (<Text fontSize="sm" color={color}>{label}</Text>
  )
}
const TabNavigator = () => {
  const { colors } = useTheme()
  const { t } = useTranslation()
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
      // eslint-disable-next-line react/prop-types
        tabBarIcon: ({ focused }) => {
          switch (route.name) {
            case 'Home':
              return (
                <FontIcon
                  name="home"
                  color={focused ? colors.dark['50'] : colors.gray}
                  size={20}
                  solid
                />
              )
            case 'Profile':
              return (
                <FontIcon
                  name="user"
                  color={focused ? colors.dark['50'] : colors.gray}
                  size={20}
                  solid
                />
              )
            case 'Camera':
              return (
                <FontIcon
                  name="camera"
                  color={focused ? colors.dark['50'] : colors.gray}
                  size={20}
                  solid
                />
              )
            default:
              return <View />
          }
        },
        tabBarStyle: [
          {
            backgroundColor: '#f4511e',
            display: 'flex',
          },
          null,
        ],
      })}
      initialRouteName="Home"
      swipeEnabled={false}
    >
      <Tab.Screen
        name="Home"
        component={HomeScreen}
        options={
        () => ({
          title: 'home',
          headerShown: false,
          tabBarLabel: ({ focused }) => renderTabBarLabel(focused, t('home')),
        })
        }
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={
        () => ({
          title: 'Profile',
          headerShown: false,
          tabBarLabel: ({ focused }) => renderTabBarLabel(focused, t('profile')),
        })
        }
      />
      <Tab.Screen
        name="Camera"
        component={CameraScreen}
        options={
        () => ({
          title: 'Camera',
          headerShown: false,
          tabBarLabel: ({ focused }) => renderTabBarLabel(focused, t('cameraScreen.label')),
        })
        }
      />

    </Tab.Navigator>
  )
}
export default TabNavigator
