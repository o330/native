import React from 'react'
import {
  createDrawerNavigator, DrawerContentScrollView, DrawerItem, DrawerItemList,
} from '@react-navigation/drawer'
import { Icon, Text, View } from 'native-base'
import { Entypo } from '@expo/vector-icons'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'
import DrawerMenu from './DrawerMenu'
import { useAuthContext } from '../../../context/auth'
import TabNavigator from './TabNavigator'
import SettingsScreen from '../../../scenes/settings'
import EditCategoriesForm from '../../../scenes/expense/category/EditCategoriesForm'

const Drawer = createDrawerNavigator()

const DrawerMenuContainer = ({ signOut, ...props }) => {
  const { t } = useTranslation()
  return (
    <View flex={1}>
      <DrawerContentScrollView {...props}>
        <DrawerMenu {...props} />
        <DrawerItemList {...props} />
      </DrawerContentScrollView>
      <DrawerItem
        label={() => (<Text>{t('logOut')}</Text>)}
        onPress={signOut}
        icon={(focused, color, size) => (
          <Icon
            color={color}
            size={size}
            name={focused ? 'log-out' : 'heart-outline'}
            as={Entypo}
          />
        )}
      />
    </View>
  )
}
const DrawerNavigator = () => {
  const { t } = useTranslation()
  const { signOut, dispatch } = useAuthContext()
  const getOptions = ({ labelName, ...rest }) => {
    const label = t(labelName)
    return {
      title: label,
      drawerLabel: () => <Text>{label}</Text>,
      ...rest,
    }
  }
  return (
    <Drawer.Navigator
      initialRouteName="HomeTabNavigator"
      drawerContent={(props) => <DrawerMenuContainer signOut={() => signOut(dispatch)} {...props} />}
      screenOptions={{
        headerStyle: {
          backgroundColor: '#f4511e',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
      }}
    >
      <Drawer.Screen
        name="HomeTabNavigator"
        component={TabNavigator}
        options={{
          drawerItemStyle: {
            display: 'none',
          },
          title: 'Onion',
        }}
      />
      <Drawer.Screen
        name="EditCategoriesForm"
        component={EditCategoriesForm}
        options={getOptions({ labelName: 'createCategoryForm.screenTitle', unmountOnBlur: true })}
      />

      <Drawer.Screen
        name="Settings"
        component={SettingsScreen}
        options={getOptions({ labelName: 'settings' })}
      />
    </Drawer.Navigator>
  )
}

export default DrawerNavigator

DrawerMenuContainer.propTypes = {
  signOut: PropTypes.func,
}

DrawerMenuContainer.defaultProps = {
  signOut: () => null,
}
