const arrayWithUpdatedElement = (element, index, array) => array.map((old, idx) => {
  if (idx === index) {
    return element
  } return old
})
export { arrayWithUpdatedElement }
