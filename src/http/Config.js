import axios from 'axios'
import i18n from '../i18n/language'
import { log } from '../components/error'

axios.defaults.baseURL = 'http://192.168.0.88:8080'
axios.defaults.headers.common.Authorization = null
axios.defaults.headers.post['Content-Type'] = 'application/json'
axios.defaults.timeout = 3000
axios.interceptors.request.use((config) => {
  config.params = { ...config.params, lang: i18n.language }
  return config
},
(error) => {
  log(error)
  return Promise.reject(error)
})
// Add a response interceptor
axios.interceptors.response.use((response) => response,
  (error) => {
    log(error)
    return Promise.reject(error)
  })
export default axios
