import 'intl'
import 'intl/locale-data/jsonp/pl'
import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'
import { NativeModules } from 'react-native'
import en from '../../resource/en.json'
import pl from '../../resource/pl.json'

const locale = NativeModules.I18nManager.localeIdentifier.split('_')[0]
export const currencyFormat = new Intl.NumberFormat('pl-PL', { style: 'currency', currency: 'PLN' })
i18n.use(initReactI18next).init({
  lng: locale,
  fallbackLng: 'en',
  compatibilityJSON: 'v3',
  resources: {
    en,
    pl,
  },
  interpolation: {
    escapeValue: false, // react already safes from xss
  },
})

export default i18n
