import './src/http/Config'
import * as React from 'react'
import { NativeBaseProvider } from 'native-base'
import Root from './src/routes/navigation'
import { baseTheme } from './src/theme'

export default function App() {
  return (
    <NativeBaseProvider theme={baseTheme}>
      <Root />
    </NativeBaseProvider>
  )
}
